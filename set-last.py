#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import absolute_import, division, print_function

from os import mkdir
from os.path import isdir, isfile
from typing import Tuple
import warnings

from ansible.module_utils.basic import AnsibleModule
from appdirs import user_cache_dir
from yaml import Loader, dump, load

__metaclass__ = type

DOCUMENTATION = r'''
---
module: set-last

short_description: Setting user input as last used value to system storage

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: |
    This module would try to set the input value of a variable to a cache file. If the file does not exists, it would be created with the said key-value pair.

options:
    namespace:
        description: This is the namespace of the variable. It is used as the cache file name. A default namespace would be used if none is given.
        required: false.
        type: str
        default: "smarter-input"
    name:
        description: This is the identifier of the variable. It is used as key inside the cache file. 
        required: true
        type: str
    value:
        description: This is the value of the variable to be stored.
        required: true
        type: str
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - smarter-input-helper.get-last

author:
    - Linghao Zhang (@zoeng-linghao)
'''

EXAMPLES = r'''
# Save a variable to storage
- name: Saving a variable
  smarter-input-helper.get-last:
    namespace: test_ns
    name: test_name
    value: test
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
new:
    description: If the variable is created in the cache
    type: bool
    returned: always
    sample: True
'''


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        namespace=dict(type='str', required=False, default='smarter-input'),
        name=dict(type='str', required=True),
        value=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        new=False
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # Write value to cache file
    result["new"], result["changed"] = write_cache_file(module.params["namespace"], module.params["name"], module.params["value"])

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def get_cache_file(namespace: str) -> Tuple[str, str]:
    """
    Return the directory and filename of the cache

    :param  namespace   The namespace of this module instance
    :return cache_dir   Directory of the cache
    :return cahce_file  Filename of the cache
    """
    cache_dir = user_cache_dir(appname="ansible.smarter-input")
    cache_file = cache_dir + "/" + namespace + ".yaml"

    return (cache_dir, cache_file)


def write_cache_file(namespace: str, name: str, value: str) -> Tuple[bool, bool]:
    cache_dir, cache_file = get_cache_file(namespace)
    new = False
    changed = False

    try:
        # If directory does not exist
        if not isdir(cache_dir):
            mkdir(cache_dir)
            new = True
            changed = True

        # if file does not exist
        if not isfile(cache_file):
            with open(cache_file, "w") as fp:
                dump({name: value}, fp)
            new = True
            changed = True

        else:
            with open(cache_file, "r") as fp:
                stored_values = load(fp, Loader=Loader)

            new = not name in stored_values
            changed = not stored_values[name] == value

            stored_values[name] = value

            with open(cache_file, "w") as fp:
                dump(stored_values, fp)

        return new, changed

    except PermissionError:
        warnings.warn("Permission denied writing to cache.")
        return False, False

def main():
    run_module()


if __name__ == '__main__':
    main()
